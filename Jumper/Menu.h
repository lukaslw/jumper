#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <math.h>
#include <vector>
#include <fstream>
#include <iostream>
#include <sstream>

using namespace sf;
using namespace std;

extern RenderWindow window;

struct score {
    string nick;
    int points;
};

class Menu {
    bool menuActive;
    bool highscoreActive;
    bool optionsActive;
    int mouseX;
    int mouseY;
    int menuSize;
    int menuOptionX;
    int menuOptionY;
    int menuYOffset;
    int menuButtonXSize;
    int menuButtonYSize;
    int backOptionX;
    int backOptionY;
    int backButtonXSize;
    int backButtonYSize;
    Music MusicMenuObject;
    Texture backgroundTexture;
    Texture cursorTexture;
    Texture startTexture;
    Texture optionTexture;
    Texture highscoreTexture;
    Texture titleTexture;
    Texture faceTexture;
    Texture backTexture;
    Texture quitTexture;
    Texture yournameTexture;
    Texture wsadTexture;
    Texture arrowTexture;
    Texture controlTexture;
    Texture gameoverTexture;
    Texture musicTexture;
    Texture onTexture;
    Texture offTexture;
    vector<score> vectorScores;
public:
    int control;
    bool musicActive;
    Menu();
    void loadTextures();
    void loadMusic();
    void stopMusic();
    void showMenu();
    void drawMenu();
    void drawTitle();
    void showHighscore(Font fontObject);
    void drawHighscore(Font fontObject);
    void loadHighscore();
    void sortHighscore();
    void saveHighscore();
    void newScore(int points, Font objectFont);
    void showOptions();
    void drawOptions();
    void setOptions();
    void mousePosition();
    void menuClick();
    void optionsClick();
    void backClick();
    void backHighlight();
    void drawCursor();
    void menuHighlight();
    bool isActive(){
        return menuActive;
    }
    bool isHighscoreActive(){
        return highscoreActive;
    }
    bool isOptionsActive(){
        return optionsActive;
    }
};