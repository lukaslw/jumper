#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <math.h>
#include <vector>
#include <iostream>
#include <sstream>

using namespace sf;
using namespace std;

class Char {
    friend class Map;
    bool leftP;
    bool rightP;
public:
    Char();
    vector<Texture> jumperRightRun;
    vector<Texture> jumperLeftRun;
    Texture jumperStandLeft;
    Texture jumperStandRight;
    int life; //ilosc zyc
    double pX, pY; //polozenie w pixelach
    double orgpX, orgpY; //polozenie poczatkowe
    double v; //predkosc
    double a; //przyspieszenie w gore(?)
    float airTime; //czas w powietrzu(?)
    float jumpTime; //moment ostatniego skoku
    float stopRisingTime; //moment zatrzymania w powietrzu
    float startFallingTime; //moment rozpoczecia spadania
    float risingV; //predkosc w trakcie wznoszenia
    float fallingV; //predkosc w trakcie spadania
    bool isRising; //czy sie wznosi
    bool isFalling; //czy opada
    bool stopRising; //czy koniec spadania
    bool leftJump; //czy leci w lewo
    bool rightJump; //czy leci w prawo
    int points; //punkty
    int move(float gameTime, Map &objectMap, int &levelActive, float deltaTime); //poruszenie
    void jump(Map &objectMap, float gameTime, Sound &jumpSound); //skok w gore
    float inAir(Map &objectMap, float gameTime); //czy w powietrzu
    void leftPressed();
    void rightPressed(); 
    void leftReleased();
    void rightReleased();
    void airTimer();
    void falling(float gameTime, float startFallingTime); //spadanie
    void rising(Map &objectMap, float gameTime);
    void catchPoint(Map &objectMap); //zbieranie punktow
    void catchLife(Map &objectMap); //zbieranie �y�
    void spikesDmg(Map &objectMap); //kolce
    bool teleportPlayer(Map &objectMap, int &levelActive);
    void drawPoints(Font ArialFont);
    void loadTextures();
    void loadJumperRightRun();
    void loadJumperLeftRun();
    bool lastDirection; //w lewo - 0; w prawo - 1
    int jumperFrame;
    float timeJumperFrame;
    void nextFrame(int &frame);
    void drawPlayer(double x, double y, float deltaTime);
};