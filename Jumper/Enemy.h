#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <math.h>
#include <vector>
#include <iostream>
#include <sstream>

using namespace sf;
using namespace std;

class Enemy{
    friend class Char;
    friend class Map;
public:
    Enemy();
    vector<Texture> enemyRightRun;
    vector<Texture> enemyLeftRun;
    Texture enemyStandLeft;
    Texture enemyStandRight;
    double pX, pY;
    double orgpX, orgpY;
    bool leftP, rightP;
    double v;
    bool lastDirection; //w lewo - 0; w prawo - 1
    float timeEnemyFrame;
    int enemyFrame;
    void findPlayer(Char &objectJumper, Map &objectMap, bool follow);
    void playerColision(Char &objectJumper);
    int inAir(Map &objectMap);
    int onLeft(Map &objectMap);
    int onRight(Map &objectMap);
    int isWall(Char &objectJumper, Map &objectMap);
    void move(float gameTime, Map &objectMap, Char &objectJumper, int &levelActive, float deltaTime, bool follow);
    void nextFrame(int &frame, int amount);
    void drawEnemy(float x, float y, float deltaTime);
};

class BlueEnemy : public Enemy{
    void loadBlueEnemyLeftRun();
    void loadBlueEnemyRightRun();
public:
    void loadBlueEnemyTextures();
};

class GreenEnemy : public Enemy{
    void loadGreenEnemyLeftRun();
    void loadGreenEnemyRightRun();
public:
    void loadGreenEnemyTextures();
};

class Boss : public Enemy{
    int r;
    Texture bulletBoss;
    vector<Sprite> bulletsVector;
public:
    Boss();
    void throwBullets(Char &objectChar);
    void loadBossTextures();
    void drawBoss(float x, float y, float deltaTime, Char &objectChar);
};