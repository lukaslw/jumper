#include "Enemy.h"
#include "Char.h"
#include "Map.h"

Enemy::Enemy():v(60),pX(0),pY(0),orgpX(0),orgpY(0),leftP(1),rightP(0),lastDirection(0),timeEnemyFrame(0),enemyFrame(0)
{
}

int Enemy::inAir(Map &objectMap){
    if(objectMap.value(pX/20, (pY/20)+1) != 'X' && objectMap.value(pX/20, (pY/20)+1) != 'G' && objectMap.value(pX/20, (pY/20)+1) != 'Z'){
        return 1;
    }
    else{
        return 0;
    }
}

int Enemy::onLeft(Map &objectMap){
    if(objectMap.value(pX/20, pY/20) == 'X' || objectMap.value(pX/20, pY/20) == 'G' || objectMap.value(pX/20, pY/20) == 'Z'){
        return 1;
    }
    else{
        return 0;
    }
}

int Enemy::onRight(Map &objectMap){

    if(objectMap.value((pX/20)+1, pY/20) == 'X' || objectMap.value((pX/20)+1, pY/20) == 'G' || objectMap.value((pX/20)+1, pY/20) == 'Z'){
        return 1;
    }
    else{
        return 0;
    }
}

int Enemy::isWall(Char &objectJumper, Map &objectMap){
    
    int playerX = objectJumper.pX/20;
    int enemyX = pX/20;

    if(enemyX < playerX){
        playerX += 1;
        if(pX/20 - 1 > 0){
            enemyX -= 1;
        }
        for(int i = enemyX; i < playerX; i++){
            if(objectMap.value(i, pY/20) == 'X' || objectMap.value(i, pY/20) == 'G' || objectMap.value(i, pY/20) == 'Z'){
                return 1;
            }
        }
    }
    else{
        if(objectJumper.pX/20 - 1 > 0){
            playerX -= 1;
        }
        enemyX += 1;
        for(int i = playerX; i < enemyX; i++){
            if(objectMap.value(i, pY/20) == 'X' || objectMap.value(i, pY/20) == 'G' || objectMap.value(i, pY/20) == 'Z'){
                return 1;
            }
        }
    }
    return 0;
}

void Enemy::findPlayer(Char &objectJumper, Map &objectMap, bool follow){

    if(abs(objectJumper.pY - pY) < 25 && isWall(objectJumper, objectMap) == 0 && follow == 1 && inAir(objectMap) == 0){
        
        if(objectJumper.pX > pX){
            leftP = 0;
            rightP = 1;
            lastDirection = 1;
        }
        else{
            leftP = 1;
            rightP = 0;
            lastDirection = 0;
        }
    }
    else{
        if((pX < 1 || inAir(objectMap) == 1 || onLeft(objectMap) == 1) && lastDirection == 0 ){
            leftP = 0;
            rightP = 1;
            lastDirection = 1;
        }
        else if((pX > 779 || inAir(objectMap) == 1 || onRight(objectMap) == 1) && lastDirection == 1){
            leftP = 1;
            rightP = 0;
            lastDirection = 0;
        }
    }
}

void Enemy::playerColision(Char &objectJumper){
    int enemyX = pX/20;
    int enemyY = pY/20;
    int playerX = objectJumper.pX/20;
    int playerY = objectJumper.pY/20;
    if(enemyX == playerX && enemyY == playerY){
        objectJumper.life--;
        objectJumper.pX = objectJumper.orgpX;
        objectJumper.pY = objectJumper.orgpY;
        pX = orgpX;
        pY = orgpY;
    }
}

void Enemy::move(float gameTime, Map &objectMap, Char &objectJumper, int &levelActive, float deltaTime, bool follow){
    
    //podazanie za graczem
    findPlayer(objectJumper, objectMap, follow);

    //poruszenie w lewo
    if(leftP == 1){
        if(objectMap.value(pX/20, pY/20) != 'X' && objectMap.value(pX/20, pY/20) != 'G' && objectMap.value(pX/20, pY/20) != 'Z'){
            pX -= v*deltaTime;
        }
    }
    //poruszenie w prawo
    if(rightP == 1){
        if(objectMap.value((pX/20)+1, pY/20) != 'X' && objectMap.value((pX/20)+1, pY/20) != 'G' && objectMap.value((pX/20)+1, pY/20) != 'Z'){
            pX += v*deltaTime;
        }
    }

    //kolizja z graczem
    playerColision(objectJumper);
}

//Dla wyswietlania animacji
void Enemy::nextFrame(int &frame, int amount){
    if(frame < amount - 1){
        frame++;
    }
    else{
        frame = 0;
    }
}

//Wyswietlenie przeciwnika
void Enemy::drawEnemy(float x, float y, float deltaTime){
    Sprite enemy;

    timeEnemyFrame += deltaTime;

    if(timeEnemyFrame > 0.05){
        timeEnemyFrame = 0;
        nextFrame(enemyFrame, 6);
    }
    if(rightP == 1){
        enemy.setTexture(enemyRightRun[enemyFrame], 0);
    }
    else if(leftP == 1){
        enemy.setTexture(enemyLeftRun[enemyFrame], 0);
    }
    enemy.setPosition(x, y);
    window.draw(enemy);
}


//Wyswietlenie bossa
//Wyswietlenie przeciwnika
void Boss::drawBoss(float x, float y, float deltaTime, Char &objectChar){
    Sprite enemy;

    timeEnemyFrame += deltaTime;

    if(timeEnemyFrame > 0.20){
        r += 20;
        if(r > 300){
            r = 0;
        }
        timeEnemyFrame = 0;
        nextFrame(enemyFrame, 3);
    }
    if(rightP == 1){
        enemy.setTexture(enemyRightRun[enemyFrame], 0);
    }
    else if(leftP == 1){
        enemy.setTexture(enemyLeftRun[enemyFrame], 0);
    }
    enemy.setPosition(x-21, y-32);
    window.draw(enemy);
    throwBullets(objectChar);
}
//Tekstury dla animacji postaci
void BlueEnemy::loadBlueEnemyRightRun(){
    Texture enemyTexture;
    enemyTexture.loadFromFile("png/blueEnemyRight/1.png");
    enemyRightRun.push_back(enemyTexture);
    enemyTexture.loadFromFile("png/blueEnemyRight/2.png");
    enemyRightRun.push_back(enemyTexture);
    enemyTexture.loadFromFile("png/blueEnemyRight/3.png");
    enemyRightRun.push_back(enemyTexture);
    enemyTexture.loadFromFile("png/blueEnemyRight/4.png");
    enemyRightRun.push_back(enemyTexture);
    enemyTexture.loadFromFile("png/blueEnemyRight/5.png");
    enemyRightRun.push_back(enemyTexture);
    enemyTexture.loadFromFile("png/blueEnemyRight/6.png");
    enemyRightRun.push_back(enemyTexture);
}

void BlueEnemy::loadBlueEnemyLeftRun(){
    Texture enemyTexture;

    enemyTexture.loadFromFile("png/blueEnemyLeft/1.png");
    enemyLeftRun.push_back(enemyTexture);
    enemyTexture.loadFromFile("png/blueEnemyLeft/2.png");
    enemyLeftRun.push_back(enemyTexture);
    enemyTexture.loadFromFile("png/blueEnemyLeft/3.png");
    enemyLeftRun.push_back(enemyTexture);
    enemyTexture.loadFromFile("png/blueEnemyLeft/4.png");
    enemyLeftRun.push_back(enemyTexture);
    enemyTexture.loadFromFile("png/blueEnemyLeft/5.png");
    enemyLeftRun.push_back(enemyTexture);
    enemyTexture.loadFromFile("png/blueEnemyLeft/6.png");
    enemyLeftRun.push_back(enemyTexture);
}

void BlueEnemy::loadBlueEnemyTextures(){
    loadBlueEnemyRightRun();
    loadBlueEnemyLeftRun();
}

//Tekstury dla animacji postaci
void GreenEnemy::loadGreenEnemyRightRun(){
    Texture enemyTexture;
    enemyTexture.loadFromFile("png/greenEnemyRight/1.png");
    enemyRightRun.push_back(enemyTexture);
    enemyTexture.loadFromFile("png/greenEnemyRight/2.png");
    enemyRightRun.push_back(enemyTexture);
    enemyTexture.loadFromFile("png/greenEnemyRight/3.png");
    enemyRightRun.push_back(enemyTexture);
    enemyTexture.loadFromFile("png/greenEnemyRight/4.png");
    enemyRightRun.push_back(enemyTexture);
    enemyTexture.loadFromFile("png/greenEnemyRight/5.png");
    enemyRightRun.push_back(enemyTexture);
    enemyTexture.loadFromFile("png/greenEnemyRight/6.png");
    enemyRightRun.push_back(enemyTexture);
}

void GreenEnemy::loadGreenEnemyLeftRun(){
    Texture enemyTexture;
    enemyTexture.loadFromFile("png/greenEnemyLeft/1.png");
    enemyLeftRun.push_back(enemyTexture);
    enemyTexture.loadFromFile("png/greenEnemyLeft/2.png");
    enemyLeftRun.push_back(enemyTexture);
    enemyTexture.loadFromFile("png/greenEnemyLeft/3.png");
    enemyLeftRun.push_back(enemyTexture);
    enemyTexture.loadFromFile("png/greenEnemyLeft/4.png");
    enemyLeftRun.push_back(enemyTexture);
    enemyTexture.loadFromFile("png/greenEnemyLeft/5.png");
    enemyLeftRun.push_back(enemyTexture);
    enemyTexture.loadFromFile("png/greenEnemyLeft/6.png");
    enemyLeftRun.push_back(enemyTexture);
}

void GreenEnemy::loadGreenEnemyTextures(){
    loadGreenEnemyRightRun();
    loadGreenEnemyLeftRun();
}

Boss::Boss():r(0)
{
}

void Boss::loadBossTextures(){
    //animacja
    Texture enemyTexture;
    //lewo
    enemyTexture.loadFromFile("png/boss/left/1.png");
    enemyLeftRun.push_back(enemyTexture);
    enemyTexture.loadFromFile("png/boss/left/2.png");
    enemyLeftRun.push_back(enemyTexture);
    enemyTexture.loadFromFile("png/boss/left/3.png");
    enemyLeftRun.push_back(enemyTexture);
    //prawo
    enemyTexture.loadFromFile("png/boss/right/1.png");
    enemyRightRun.push_back(enemyTexture);
    enemyTexture.loadFromFile("png/boss/right/2.png");
    enemyRightRun.push_back(enemyTexture);
    enemyTexture.loadFromFile("png/boss/right/3.png");
    enemyRightRun.push_back(enemyTexture);
    //pocisk
    bulletBoss.loadFromFile("png/boss/bullet.png");
}

void Boss::throwBullets(Char &objectChar){
    Sprite bullet;
    bullet.setTexture(bulletBoss);
    int xBoss = pX/20;
    int yBoss = pY/20;
    int xPlayer = objectChar.pX/20;
    int yPlayer = objectChar.pY/20;
    int R = r/20;

    for(int y = 0; y < 30; y++){
        for(int x = 0; x < 40; x++){
            int a = pow((x - xBoss), 2.0) + pow((y - yBoss), 2.0);
            int b = pow(R, 2.0);
            int c = pow(R+1, 2.0);
            if(a > b && a < c){
                if(x%2 == 0){
                    //bullet.setPosition(x*20, y*20-32);
                    bullet.setPosition(x*20, y*20);
                    window.draw(bullet);
                    if(xPlayer == x && yPlayer == y){
                        objectChar.life--;
                        objectChar.pX = objectChar.orgpX;
                        objectChar.pY = objectChar.orgpY;
                    }
                }
            }
        }
    }
}