#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <math.h>
#include <vector>
#include <iostream>
#include <sstream>

using namespace sf;
using namespace std;

extern RenderWindow window;

class Char;
class Enemy;

class Map {
    char **Allocate2DArray(int nRows, int nCols);
    void Free2DArray(char** Array);
    void drawSquare(float x, float y);
    void drawCircle(float x, float y);
    void drawPlayer(float x, float y);
    void drawTimer(float gameTime, Font ArialFont);
    void drawBackground(int &levelActive);
    void drawPoints(Char &objectChar, Font objectFont);
    void drawLife(Char &objectChar);
    void drawMap(Char &objectChar, float deltaTime);
    char **map;
    vector<Texture> coinsLoad;
	vector<Texture> portalLoad;
    Texture groundTexture1;
    Texture groundTexture2;
    Texture groundTexture3;
    Texture lifeTexture;
    Texture coinTexture;
    Texture portalTexture;
    Texture spikesLTexture;
    Texture spikesPTexture;
    Texture hpTexture;
    Texture backgroundTexture1;
    Texture backgroundTexture2;
    Texture backgroundTexture3;
    Texture backgroundTexture4;
    float timeCoinFrame;
	float timePortalFrame;
    int coinFrame;
	int portalFrame;
public:
    Map(int, int);
    ~Map();
    void drawScene(Char &objectChar, int &levelActive, float gameTime, float deltaTime, double playerX, double playerY, Font ArialFont);
    void loadLevel(int &levelActive);
    template <class T> void loadPosition(T &objectChar, char letter){
        for(int y = 0; y < 30; y++){
            for(int x = 0; x < 40; x++){
                if(map[x][y] == letter){
                    objectChar.pX = x*20;
                    objectChar.pY = y*20;
                    objectChar.orgpX = objectChar.pX;
                    objectChar.orgpY = objectChar.pY;
                    map[x][y] = '0';
                    return;
                }
            }
        }
    }
    void loadTextures();
    void nextFrame(int &frame, int amount);
    void drawCoin(int x, int y, float deltaTime);
	void drawPortal(int x, int y, float deltaTime);
	void loadCoins();
	void loadPortal();
    void change(int x, int y, char letter){
        map[x][y] = letter;
    }
    char value(int x, int y){
        return map[x][y];
    }
};
