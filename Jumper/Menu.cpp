#include "Menu.h"

Menu::Menu():menuActive(1),highscoreActive(0),optionsActive(0),control(0),musicActive(1)
{
}

void Menu::loadTextures(){
    backgroundTexture.loadFromFile("png/background.png");
    titleTexture.loadFromFile("png/title.png");
    cursorTexture.loadFromFile("png/cursor.png");
    quitTexture.loadFromFile("png/quit.png");
    startTexture.loadFromFile("png/start.png");
    optionTexture.loadFromFile("png/option.png");
    highscoreTexture.loadFromFile("png/highscore.png");
    faceTexture.loadFromFile("png/face.png");
    backTexture.loadFromFile("png/back.png");
    yournameTexture.loadFromFile("png/yourname.png");
    wsadTexture.loadFromFile("png/wsad.png");
    arrowTexture.loadFromFile("png/arrows.png");
    controlTexture.loadFromFile("png/control.png");
    gameoverTexture.loadFromFile("png/gameover.png");
    musicTexture.loadFromFile("png/music.png");
    onTexture.loadFromFile("png/on.png");
    offTexture.loadFromFile("png/off.png");
}

void Menu::loadMusic(){
    if (MusicMenuObject.openFromFile("music_menu.ogg") != 0 && musicActive == 1 && menuActive == 1){
        MusicMenuObject.play();
        MusicMenuObject.setLoop(true);
    }
}

void Menu::stopMusic(){
    MusicMenuObject.stop();
}

void Menu::showMenu(){
    drawMenu();
    mousePosition();
    drawCursor();
    //menuHighlight();
    window.display();
    window.clear();
}

void Menu::drawMenu(){
    //background
    Sprite background;
    background.setTexture(backgroundTexture, 0);
    window.draw(background);

    //face
    Sprite face;
    face.setTexture(faceTexture, 0);
    face.setPosition(580, 210);
    window.draw(face);

    //title
    Sprite title;
    title.setTexture(titleTexture, 0);
    title.setPosition(window.getSize().x/2 - title.getGlobalBounds().width/2, 110);
    window.draw(title);

    //menu
    menuSize = 4;
    menuYOffset = 15;
    menuButtonXSize = startTexture.getSize().x;
    menuButtonYSize = startTexture.getSize().y;
    menuOptionX = window.getSize().x/2 - menuButtonXSize/2;
    menuOptionY = window.getSize().y/2 - (menuSize/2)*(menuButtonYSize + menuYOffset);
    backButtonXSize = backTexture.getSize().x;
    backButtonYSize = backTexture.getSize().y;
    backOptionX = window.getSize().x/2 - backButtonXSize/2;
    backOptionY = 480;

    Sprite startText;
	startText.setTexture(startTexture, 0);
	startText.setPosition(menuOptionX, menuOptionY);
	window.draw(startText);
    
    Sprite highscoreText;
	highscoreText.setTexture(highscoreTexture, 0);
	highscoreText.move(menuOptionX, menuOptionY + menuButtonYSize + menuYOffset);
	window.draw(highscoreText);

    Sprite optionText;
	optionText.setTexture(optionTexture, 0);
    optionText.move(menuOptionX, menuOptionY + 2*menuButtonYSize + 2*menuYOffset);
    window.draw(optionText);
    
    Sprite quitText;
	quitText.setTexture(quitTexture, 0);
	quitText.move(menuOptionX, menuOptionY + 3*menuButtonYSize + 3*menuYOffset);
	window.draw(quitText);
}

void Menu::drawTitle(){

}

void Menu::showHighscore(Font fontObject){
    //go to highscore
    // - load it from file
    // - draw it
    drawHighscore(fontObject);
    mousePosition();
    drawCursor();
    //backHighlight();
    window.display();
    window.clear();
}

void Menu::drawHighscore(Font fontObject){
    //background
    Sprite background;
    background.setTexture(backgroundTexture, 0);
    window.draw(background);

    //highscore title
    Sprite highscoreText;
    highscoreText.setTexture(highscoreTexture, 0);
	highscoreText.move(window.getSize().x/2 - highscoreText.getGlobalBounds().width/2, 100);
    window.draw(highscoreText);

    //loading highscore
    string text;
    int size;
    vectorScores.size() > 9 ? size = 9 : size = vectorScores.size();

    for(int i = 0; i < size; i++){
        const char *nick = vectorScores[i].nick.c_str();
        int points = vectorScores[i].points;
        stringstream stringS1 (stringstream::in | stringstream::out);
        stringS1 << i+1;
        stringstream stringS2 (stringstream::in | stringstream::out);
        stringS2 << points;
        string player = stringS1.str() + ". " + nick + " - " + stringS2.str();
        Text playerText;
        playerText.setString(player);
        playerText.setColor(Color::White);
        playerText.setFont(fontObject);
        playerText.setCharacterSize(20);
        playerText.move(window.getSize().x/2-70, 200+i*20);
        window.draw(playerText);
    }

    //back
	Sprite backText;
    backText.setTexture(backTexture, 0);
    backText.move(window.getSize().x/2 - backText.getGlobalBounds().width/2, 480);
    window.draw(backText);
}

void Menu::showOptions(){
    //go to options
    // - load from file (?)
    // - draw it
    // - allow to change
    drawOptions();
    mousePosition();
    drawCursor();
    //backHighlight();
    window.display();
    window.clear();
}

void Menu::drawOptions(){
    //background
    Sprite background;
    background.setTexture(backgroundTexture, 0);
    window.draw(background);

    //option title
    Sprite optionText;
    optionText.setTexture(optionTexture, 0);
	optionText.move(window.getSize().x/2 - optionText.getGlobalBounds().width/2, 100);
    window.draw(optionText);

    //change options
    setOptions();

    //back
    Sprite backText;
    backText.setTexture(backTexture, 0);
    backText.move(window.getSize().x/2 - backText.getGlobalBounds().width/2, 480);
    window.draw(backText); 
}

void Menu::setOptions(){

    Sprite controlText;
	controlText.setTexture(controlTexture, 0);
	controlText.setPosition(menuOptionX, menuOptionY);
	window.draw(controlText);

    Sprite wsadText;
	wsadText.setTexture(wsadTexture, 0);
	wsadText.setPosition(menuOptionX, menuOptionY + 1*(menuButtonYSize + menuYOffset));
	window.draw(wsadText);

    Sprite arrowText;
	arrowText.setTexture(arrowTexture, 0);
	arrowText.setPosition(menuOptionX, menuOptionY + 2*(menuButtonYSize + menuYOffset));
	window.draw(arrowText);

    Sprite musicText;
    musicText.setTexture(musicTexture, 0);
	musicText.setPosition(menuOptionX, menuOptionY + 3*(menuButtonYSize + menuYOffset));
	window.draw(musicText);

    Sprite onText;
    onText.setTexture(onTexture, 0);
	onText.setPosition(menuOptionX, menuOptionY + 4*(menuButtonYSize + menuYOffset));
	window.draw(onText);

    Sprite offText;
    offText.setTexture(offTexture, 0);
	offText.setPosition(menuOptionX, menuOptionY + 5*(menuButtonYSize + menuYOffset));
	window.draw(offText);
    
}

void Menu::optionsClick(){
    if(mouseX >= menuOptionX && mouseX <= menuOptionX + menuButtonXSize && mouseY >= menuOptionY + 1*(menuButtonYSize + menuYOffset) && mouseY <= menuOptionY + 1*(menuButtonYSize + menuYOffset) + menuButtonYSize){
        control = 1;
        optionsActive = 0;
        menuActive = 1;
    }
    else if(mouseX >= menuOptionX && mouseX <= menuOptionX + menuButtonXSize && mouseY >= menuOptionY + 2*(menuButtonYSize + menuYOffset) && mouseY <= menuOptionY + 2*(menuButtonYSize + menuYOffset) + menuButtonYSize){
        control = 0;
        optionsActive = 0;
        menuActive = 1;
    }
    else if(mouseX >= menuOptionX && mouseX <= menuOptionX + menuButtonXSize && mouseY >= menuOptionY + 4*(menuButtonYSize + menuYOffset) && mouseY <= menuOptionY + 4*(menuButtonYSize + menuYOffset) + menuButtonYSize){
        optionsActive = 0;
        menuActive = 1;
        MusicMenuObject.setVolume(100);
        musicActive = 1;
    }
    else if(mouseX >= menuOptionX && mouseX <= menuOptionX + menuButtonXSize && mouseY >= menuOptionY + 5*(menuButtonYSize + menuYOffset) && mouseY <= menuOptionY + 5*(menuButtonYSize + menuYOffset) + menuButtonYSize){
        optionsActive = 0;
        menuActive = 1;
        MusicMenuObject.setVolume(0);
        musicActive = 0;
    }
}

void Menu::mousePosition(){
    Mouse MouseObject;
    Vector2i mousePostion = MouseObject.getPosition(window);
    mouseX = mousePostion.x;
    mouseY = mousePostion.y;
}

void Menu::menuClick(){
    //check position and allow to choose an option
    if(mouseX >= menuOptionX && mouseX <= menuOptionX + menuButtonXSize && mouseY >= menuOptionY && mouseY <= menuOptionY + menuButtonYSize){
        menuActive = 0;
    }
    else if(mouseX >= menuOptionX && mouseX <= menuOptionX + menuButtonXSize && mouseY >= menuOptionY + 1*(menuButtonYSize + menuYOffset) && mouseY <= menuOptionY + 1*(menuButtonYSize + menuYOffset) + menuButtonYSize){
        highscoreActive = 1;
    }
    else if(mouseX >= menuOptionX && mouseX <= menuOptionX + menuButtonXSize && mouseY >= menuOptionY + 2*(menuButtonYSize + menuYOffset) && mouseY <= menuOptionY + 2*(menuButtonYSize + menuYOffset) + menuButtonYSize){
        optionsActive = 1;
    }
    else if(mouseX >= menuOptionX && mouseX <= menuOptionX + menuButtonXSize && mouseY >= menuOptionY + 3*(menuButtonYSize + menuYOffset) && mouseY <= menuOptionY + 3*(menuButtonYSize + menuYOffset) + menuButtonYSize){
        window.close();
    }
}

void Menu::menuHighlight(){
    if(mouseX >= menuOptionX && mouseX <= menuOptionX + menuButtonXSize && mouseY >= menuOptionY && mouseY <= menuOptionY + menuButtonYSize){
        
    }
    else{
        
    }
    if(mouseX >= menuOptionX && mouseX <= menuOptionX + menuButtonXSize && mouseY >= menuOptionY + 1*(menuButtonYSize + menuYOffset) && mouseY <= menuOptionY + 1*(menuButtonYSize + menuYOffset) + menuButtonYSize){
        
    }
    else{
        
    }
    if(mouseX >= menuOptionX && mouseX <= menuOptionX + menuButtonXSize && mouseY >= menuOptionY + 2*(menuButtonYSize + menuYOffset) && mouseY <= menuOptionY + 2*(menuButtonYSize + menuYOffset) + menuButtonYSize){
        
    }
    else{
       
    }
}

void Menu::backClick(){
    if(mouseX >= backOptionX && mouseX <= backOptionX + backButtonXSize && mouseY >= backOptionY && mouseY <= backOptionY + backButtonYSize){
        menuActive = 1;
        highscoreActive = 0;
        optionsActive = 0;
    }
}

void Menu::backHighlight(){
    if(mouseX >= backOptionX && mouseX <= backOptionX + menuButtonXSize && mouseY >= backOptionY && mouseY <= backOptionY + menuButtonYSize){
        
    }
    else{

    }
}

void Menu::drawCursor(){
    Sprite cursor;
    cursor.setTexture(cursorTexture, 0);
    cursor.setPosition(mouseX, mouseY);
    window.draw(cursor);
}

//Sortowanie wynikow
void Menu::sortHighscore(){
    for(size_t x = 0; x < vectorScores.size(); x++){
        for(size_t y = 0; y < vectorScores.size()-1; y++){
            if(vectorScores[y].points < vectorScores[y+1].points){
                score temp = vectorScores[y+1];
                vectorScores[y+1] = vectorScores[y];
                vectorScores[y] = temp;
            }
        }
    }
}

//Wczytanie listy highscore z pliku do wektora
void Menu::loadHighscore(){
    fstream file;
    string nick;
    string points;
    score player;
    string filename = "HighScore.txt";
    file.open(filename.c_str(), ios::in);
    if(file.good() == true){
        while(file.eof() != true){
            getline(file, nick);
            getline(file, points);
            player.nick = nick;
            player.points = atoi(points.c_str()); //konwersja na int
            vectorScores.push_back(player);
        }
        sortHighscore();
        file.close();
    }
}

//Dodanie nowego wyniku
void Menu::newScore(int points, Font objectFont){
    Event EventObject;
    string nick;

    do{
        //background
        Sprite background;
        background.setTexture(backgroundTexture, 0);
        window.draw(background);

        //"Game Over"
        Sprite gameover;
        gameover.setTexture(gameoverTexture, 0);
        gameover.setPosition(window.getSize().x/2 - gameover.getGlobalBounds().width/2, 180);
        window.draw(gameover);

        //"Your name:"
        Sprite yourname;
        yourname.setTexture(yournameTexture, 0);
        yourname.setPosition(window.getSize().x/2 - yourname.getGlobalBounds().width/2, 230);
        window.draw(yourname);

        window.pollEvent(EventObject);
        if(EventObject.type == Event::KeyPressed){
            if(nick.size() <= 20){
                if(EventObject.key.shift == 1){
                    if(EventObject.key.code >= Keyboard::A && EventObject.key.code <= Keyboard::Z){
                        nick += EventObject.key.code + 65;
                    }
                }
                else{
                    if(EventObject.key.code >= Keyboard::A && EventObject.key.code <= Keyboard::Z){
                        nick += EventObject.key.code + 97;
                    }
                }
            }
            if(EventObject.key.code == Keyboard::BackSpace){
                if(nick.size() > 0){
                    nick.erase(nick.size()-1);
                }
            }
        }
        Text nickText;
        nickText.setFont(objectFont);
        nickText.setCharacterSize(20);
        nickText.setString(nick);
        nickText.setColor(Color::White);
        nickText.move(window.getSize().x/2 - nickText.getGlobalBounds().width/2, 260);
        window.draw(nickText);
        window.display();
        window.clear();
    }while(EventObject.key.code != Keyboard::Return && EventObject.key.code != Keyboard::Escape);

    if(EventObject.key.code == Keyboard::Return){
        score newscore;
        newscore.nick = nick;
        newscore.points = points;
        vectorScores.push_back(newscore);
        sortHighscore();
        saveHighscore();
    }
}

//Zapisz liste highscore z wektora do pliku
void Menu::saveHighscore(){
    fstream plik;
    score player;
    string nick;
    char score[128];
    string filename = "HighScore.txt";
    plik.open(filename.c_str(), ios_base::out);
    if(plik.good() == true){
        int size;
        vectorScores.size() > 9 ? size = 9 : size = vectorScores.size();
        for(int i = 0; i < size; i++){
            nick = vectorScores[i].nick;
            sprintf_s(score, "%d", vectorScores[i].points); //konwersja na char*
            plik << nick << '\n';
            plik << score;
            if(i != size - 1){
                plik << '\n';
            }
        }
        plik.flush();
        plik.close();
    }
}