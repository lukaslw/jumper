#include "Char.h"
#include "Map.h"

Char::Char():v(100),a(5),airTime(0),pX(0),pY(0),orgpX(0),orgpY(0),leftP(0),rightP(0),fallingV(15),startFallingTime(0),isRising(0),stopRising(1),points(0),life(3),timeJumperFrame(0),jumperFrame(0),lastDirection(1)
{
}

int Char::move(float gameTime, Map &objectMap, int &levelActive, float deltaTime){

    //�apanie punkt�w
    catchPoint(objectMap);

    //zbierania �y�
    catchLife(objectMap);

    //wpadanie w kolce
    spikesDmg(objectMap);
 

    //poruszanie po ziemi
    if(inAir(objectMap, gameTime) == 0){
        //poruszenie w lewo
        if(leftP == 1){
            if(pX > 1 && objectMap.value(pX/20, pY/20) != 'X' && objectMap.value(pX/20, pY/20) != 'G' && objectMap.value(pX/20, pY/20) != 'Z'){
                pX -= v*deltaTime;
            }
        }
        //poruszenie w prawo
        if(rightP == 1){
            if(pX < 779 && objectMap.value((pX/20)+1, pY/20) != 'X' && objectMap.value((pX/20)+1, pY/20) != 'G' && objectMap.value((pX/20)+1, pY/20) != 'Z'){
                pX += v*deltaTime;
            }
        }
    }
    //lewo, prawo w powietrzu
    else{
        //kontynuacja lotu
        if(leftP == 1 && rightJump != 1){
            leftJump = 1;
        }
        if(rightP == 1 && leftJump != 1){
            rightJump = 1;
        }

        //poruszenie w lewo
        if(leftJump == 1){
            if(pX > 1 && objectMap.value(pX/20, pY/20) != 'X' && objectMap.value(pX/20, pY/20) != 'G' && objectMap.value(pX/20, pY/20) != 'Z'){
                pX -= v*deltaTime;
            }
        }

        //poruszenie w prawo
        if(rightJump == 1){
            if(pX < 779 && objectMap.value((pX/20)+1, pY/20) != 'X' && objectMap.value((pX/20)+1, pY/20) != 'G' && objectMap.value((pX/20)+1, pY/20) != 'Z'){
                pX += v*deltaTime;
            }
        }

        //hamowanie w locie
        if(leftJump == 1 && rightP == 1){
            pX += (v/3)*deltaTime;
        }

        if(rightJump == 1 && leftP == 1){
            pX -= (v/3)*deltaTime;
        }
    }

    //wznoszenie do gory
    if(isRising == 1){
        rising(objectMap, gameTime);
    }
    
    //moment rozpoczecia spadania
    if(startFallingTime == 0){
        startFallingTime = inAir(objectMap, gameTime);
    }
    
    //spadanie w dol
    if(startFallingTime != 0){
        if(isRising != 1){
            //skonczyl wznoszenie
            if(stopRising == 1){
                falling(gameTime, stopRisingTime);
            }
            //zsuniecie z p�ki
            else{
                falling(gameTime, startFallingTime);
            }    
        }
    }

    //wyladowal
    if(inAir(objectMap, gameTime) == 0){
        startFallingTime = 0;
        stopRising = 0;
    }

    //wejscie w teleport
    if(teleportPlayer(objectMap, levelActive) == 1){
        return 1;
    }
    return 0;
}

float Char::inAir(Map &objectMap, float gameTime){
    if(objectMap.value(pX/20, (pY/20)+1) != 'X' && objectMap.value(pX/20, (pY/20)+1) != 'G' && objectMap.value(pX/20, (pY/20)+1) != 'Z'){
        return gameTime;
    }
    else{
        return 0;
    }
}

void Char::falling(float gameTime, float startFallingTime){
        airTime = gameTime - startFallingTime;
        double s = fallingV*airTime;
        pY += s;
}

void Char::rising(Map &objectMap, float gameTime){
    airTime = gameTime - jumpTime;
    risingV -= 4.905*airTime;
    double s =  risingV*airTime;
    pY -= s;
    if(s < 0 || objectMap.value((pX+10)/20, pY/20) == 'X' || objectMap.value((pX+10)/20, pY/20) == 'G' || objectMap.value((pX+10)/20, pY/20) == 'Z'){
        isRising = 0;
        stopRising = 1;
        stopRisingTime = gameTime;
    }
}

void Char::jump(Map &objectMap, float gameTime, Sound &jumpSound){
    if(inAir(objectMap, gameTime) == 0){
        jumpTime = gameTime;
        isRising = 1;
        risingV = 30;
        leftJump = leftP;
        rightJump = rightP;
        jumpSound.play();
    }
}

void Char::leftPressed(){
    if(rightP != 1){
        leftP = 1;
        lastDirection = 0;
    }
}

void Char::leftReleased(){
    leftP = 0;
}

void Char::rightPressed(){
    if(leftP != 1){
        rightP = 1;
        lastDirection = 1;
    }
}

void Char::rightReleased(){
    rightP = 0;
}

void Char::catchPoint(Map &objectMap){
    if(objectMap.value(pX/20, pY/20) == '$'){
        objectMap.change(pX/20, pY/20, '0');
        points += 10;
    }
}

void Char::catchLife(Map &objectMap){
    if(objectMap.value(pX/20, pY/20) == 'L'){
        if(life < 3){
            objectMap.change(pX/20, pY/20, '0');
            life++;
        }
    }
}

void Char::spikesDmg(Map &objectMap){
    if(objectMap.value(pX/20, pY/20) == 'S' || objectMap.value(pX/20, pY/20) == 'D'){
        life--;
        pX = orgpX;
        pY = orgpY;
    }
}

bool Char::teleportPlayer(Map &objectMap, int &levelActive){
    if(objectMap.value(pX/20, pY/20) == 'T'){
        if(levelActive != 4){
            levelActive++;
        }
        else{
            levelActive = 1; //game over
        }
        return 1;
    }
    return 0;
}

//Tekstury dla animacji postaci
void Char::loadJumperRightRun(){
    Texture jumperTexture;

    jumperTexture.loadFromFile("png/runright/1.png");
    jumperRightRun.push_back(jumperTexture);
    jumperTexture.loadFromFile("png/runright/2.png");
    jumperRightRun.push_back(jumperTexture);
    jumperTexture.loadFromFile("png/runright/3.png");
    jumperRightRun.push_back(jumperTexture);
    jumperTexture.loadFromFile("png/runright/4.png");
    jumperRightRun.push_back(jumperTexture);
    jumperTexture.loadFromFile("png/runright/5.png");
    jumperRightRun.push_back(jumperTexture);
    jumperTexture.loadFromFile("png/runright/6.png");
    jumperRightRun.push_back(jumperTexture);
    jumperTexture.loadFromFile("png/runright/7.png");
    jumperRightRun.push_back(jumperTexture);
    jumperTexture.loadFromFile("png/runright/8.png");
    jumperRightRun.push_back(jumperTexture);
    jumperTexture.loadFromFile("png/runright/9.png");
    jumperRightRun.push_back(jumperTexture);
    jumperTexture.loadFromFile("png/runright/10.png");
    jumperRightRun.push_back(jumperTexture);
}

void Char::loadJumperLeftRun(){
    Texture jumperTexture;

    jumperTexture.loadFromFile("png/runleft/1.png");
    jumperLeftRun.push_back(jumperTexture);
    jumperTexture.loadFromFile("png/runleft/2.png");
    jumperLeftRun.push_back(jumperTexture);
    jumperTexture.loadFromFile("png/runleft/3.png");
    jumperLeftRun.push_back(jumperTexture);
    jumperTexture.loadFromFile("png/runleft/4.png");
    jumperLeftRun.push_back(jumperTexture);
    jumperTexture.loadFromFile("png/runleft/5.png");
    jumperLeftRun.push_back(jumperTexture);
    jumperTexture.loadFromFile("png/runleft/6.png");
    jumperLeftRun.push_back(jumperTexture);
    jumperTexture.loadFromFile("png/runleft/7.png");
    jumperLeftRun.push_back(jumperTexture);
    jumperTexture.loadFromFile("png/runleft/8.png");
    jumperLeftRun.push_back(jumperTexture);
    jumperTexture.loadFromFile("png/runleft/9.png");
    jumperLeftRun.push_back(jumperTexture);
    jumperTexture.loadFromFile("png/runleft/10.png");
    jumperLeftRun.push_back(jumperTexture);
}

void Char::loadTextures(){
    jumperStandLeft.loadFromFile("png/jumperLeft.png");
    jumperStandRight.loadFromFile("png/jumperRight.png");
    loadJumperRightRun();
    loadJumperLeftRun();
}

//Dla wyswietlania animacji
void Char::nextFrame(int &frame){
    if(frame < 9){
        frame++;
    }
    else{
        frame = 0;
    }
}

//Wyswietlenie postaci
void Char::drawPlayer(double x, double y, float deltaTime){
    Sprite jumper;

    timeJumperFrame += deltaTime;

    if(timeJumperFrame > 0.05){
        timeJumperFrame = 0;
        nextFrame(jumperFrame);
    }
    if(rightP == 1){
        jumper.setTexture(jumperRightRun[jumperFrame], 0);
    }
    else if(leftP == 1){
        jumper.setTexture(jumperLeftRun[jumperFrame], 0);
    }
    else if(lastDirection == 1){
        jumper.setTexture(jumperStandRight, 0);
    }
    else{
        jumper.setTexture(jumperStandLeft, 0);
    }
    jumper.setPosition(x, y);
    window.draw(jumper);
}