#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <math.h>
#include <vector>
#include <iostream>
#include <sstream>
#include "Char.h"
#include "Menu.h"
#include "Enemy.h"
#include "Map.h"

using namespace sf;
using namespace std;

RenderWindow window;

int main(){
    //dzwieki
    //random klasy

    int levelActive = 1;
    Char Jumper;
    BlueEnemy EnemyObject1;
    BlueEnemy EnemyObject2;
    GreenEnemy EnemyObject3;
    GreenEnemy EnemyObject4;
    Boss BossObject;
    BossObject.v = 30;
    Map MapObject(40,30);
    Time GlobalTime;
    Clock GlobalClock;
    Font ArialFont;
    Event EventObject;
    Menu MenuObject;
    Music MusicGameObject;
    float gameTime, deltaTime, lastTime;
    
    //Utworz okno
    window.create(VideoMode(800, 600, 32), "Jumper - Programowanie Obiektowe 2013");
    window.setMouseCursorVisible(0);

    MapObject.loadLevel(levelActive);
    MapObject.loadPosition<Char>(Jumper, 'P');
    if(levelActive != 4){
        MapObject.loadPosition<Enemy>(EnemyObject1, 'E');
        MapObject.loadPosition<Enemy>(EnemyObject2, 'E');
        MapObject.loadPosition<Enemy>(EnemyObject3, 'E');
        MapObject.loadPosition<Enemy>(EnemyObject4, 'E');
    }
    else{
        MapObject.loadPosition<Enemy>(BossObject, 'B');
    }
   
    MapObject.loadTextures();
    MenuObject.loadTextures();
    Jumper.loadTextures();
    EnemyObject1.loadBlueEnemyTextures();
    EnemyObject2.loadBlueEnemyTextures();
    EnemyObject3.loadGreenEnemyTextures();
    EnemyObject4.loadGreenEnemyTextures();
    BossObject.loadBossTextures();
    
    MenuObject.loadHighscore();
    ArialFont.loadFromFile("arial.ttf");
    MenuObject.loadMusic();
    
    SoundBuffer jumpBuffer;
    jumpBuffer.loadFromFile("jump.wav");
    Sound jumpSound;
    jumpSound.setBuffer(jumpBuffer);

    while(MenuObject.isActive() && window.isOpen()){
        if(MenuObject.isHighscoreActive() != 1){
            if(MenuObject.isOptionsActive() != 1){
                MenuObject.showMenu();
            }
            else{
                MenuObject.showOptions();
            }
        }
        else{
            MenuObject.showHighscore(ArialFont);
        } 
        while (window.pollEvent(EventObject)){
            if(EventObject.type == Event::MouseButtonPressed && EventObject.mouseButton.button == Mouse::Left){
                if(MenuObject.isHighscoreActive() != 1 && MenuObject.isOptionsActive() != 1){
                    MenuObject.menuClick();
                }
                else {
                    MenuObject.optionsClick();
                    MenuObject.backClick();
                }
            }
            if(EventObject.type == Event::Closed || (EventObject.type == Event::KeyPressed && EventObject.key.code == Keyboard::Escape)){
                window.close();
                return 0;
            }
        }
    }

    //Poczatek mierzenia czasu
    GlobalTime = GlobalClock.getElapsedTime();
    lastTime = GlobalTime.asSeconds();


    //Muzyka w grze
    MenuObject.stopMusic();
    if(MusicGameObject.openFromFile("music_game.ogg") != 0 && MenuObject.musicActive == 1){
        MusicGameObject.play();
        MusicGameObject.setLoop(true);
        MusicGameObject.setVolume(35);
    }

    //Petla glowna
    while (window.isOpen()){
        GlobalTime = GlobalClock.getElapsedTime();
        gameTime = GlobalTime.asSeconds();
        deltaTime = gameTime - lastTime;
        lastTime = gameTime;
        int newlevel = 0;

        newlevel = Jumper.move(gameTime, MapObject, levelActive, deltaTime);
        if(levelActive != 4){
            EnemyObject1.move(gameTime, MapObject, Jumper, levelActive, deltaTime, 1);
            EnemyObject2.move(gameTime, MapObject, Jumper, levelActive, deltaTime, 1);
            EnemyObject3.move(gameTime, MapObject, Jumper, levelActive, deltaTime, 1);
            EnemyObject4.move(gameTime, MapObject, Jumper, levelActive, deltaTime, 1);
        }
        else{
            BossObject.move(gameTime, MapObject, Jumper, levelActive, deltaTime, 0);
        }
        
        if(newlevel == 1){
            MapObject.loadLevel(levelActive);
            MapObject.loadPosition<Char>(Jumper, 'P');
            if(levelActive != 4){
                MapObject.loadPosition<Enemy>(EnemyObject1, 'E');
                MapObject.loadPosition<Enemy>(EnemyObject2, 'E');
                MapObject.loadPosition<Enemy>(EnemyObject3, 'E');
                MapObject.loadPosition<Enemy>(EnemyObject4, 'E');
            }
            else{
                MapObject.loadPosition<Enemy>(BossObject, 'B');
            }
        }
        if(Jumper.life == 0){
            MenuObject.newScore(Jumper.points, ArialFont);
            window.close();
        }

        //Wyświetlenie sceny
        MapObject.drawScene(Jumper, levelActive, gameTime, deltaTime, Jumper.pX, Jumper.pY, ArialFont);
        Jumper.drawPlayer(Jumper.pX, Jumper.pY, deltaTime);
        if(levelActive != 4){
            EnemyObject1.drawEnemy(EnemyObject1.pX, EnemyObject1.pY, deltaTime);
            EnemyObject2.drawEnemy(EnemyObject2.pX, EnemyObject2.pY, deltaTime);
            EnemyObject3.drawEnemy(EnemyObject3.pX, EnemyObject3.pY, deltaTime);
            EnemyObject4.drawEnemy(EnemyObject4.pX, EnemyObject4.pY, deltaTime);
        }
        else{
            BossObject.drawBoss(BossObject.pX, BossObject.pY, deltaTime, Jumper);
        }
        window.display();
        window.clear();

        while (window.pollEvent(EventObject)){
            switch (EventObject.type){
            case Event::Closed:
                window.close();
            break;
            case Event::KeyPressed:
                if(MenuObject.control == 0){
                    switch(EventObject.key.code){
                        case Keyboard::Space:
                            Jumper.jump(MapObject, gameTime, jumpSound);
                        break;
                        case Keyboard::Left:
                            Jumper.leftPressed();
                        break;
                        case Keyboard::Right:
                            Jumper.rightPressed();
                        break;
                        case Keyboard::Escape:
                            window.close();
                        break;
                        default:
                        break;
                    }
                }
                else{
                    switch(EventObject.key.code){
                        case Keyboard::W:
                            Jumper.jump(MapObject, gameTime, jumpSound);
                        break;
                        case Keyboard::A:
                            Jumper.leftPressed();
                        break;
                        case Keyboard::D:
                            Jumper.rightPressed();
                        break;
                        case Keyboard::Escape:
                            window.close();
                        break;
                        default:
                        break;
                    }
                }
            break;
            case Event::KeyReleased:
                if(MenuObject.control == 0){
                    switch(EventObject.key.code){
                        case Keyboard::Left:
                            Jumper.leftReleased();
                        break;
                        case Keyboard::Right:
                            Jumper.rightReleased();
                        break;
                        default:
                        break;
                    }
                }
                else{
                    switch(EventObject.key.code){
                        case Keyboard::A:
                            Jumper.leftReleased();
                        break;
                        case Keyboard::D:
                            Jumper.rightReleased();
                        break;
                        default:
                        break;
                    }
                }
            break;
            default:
            break;
            }
        }
    }
    return 0;
} 