#include "Map.h"
#include "Char.h"

char **Map::Allocate2DArray(int nRows, int nCols){
    //(step 1) allocate memory for array of elements of column
    char **ppi = new char*[nRows];

    //(step 2) allocate memory for array of elements of each row
    char *curPtr = new char [nRows * nCols];

    // Now point the pointers in the right place
    for(int i = 0; i < nRows; ++i){
        *(ppi + i) = curPtr;
         curPtr += nCols;
    }
    return ppi;
}

void Map::Free2DArray(char** Array){
    delete [] *Array;
    delete [] Array;
}

Map::Map(int x_size, int y_size){
    this -> map = Allocate2DArray(x_size, y_size);
    timeCoinFrame = 0;
	timePortalFrame=0;
    coinFrame = 0;
	portalFrame=0;
}

Map::~Map(){
	Free2DArray(map);
}

//Wczytanie tekstur dla rozgrywki
void Map::loadTextures(){
    groundTexture1.loadFromFile("png/ground1.png");
    groundTexture2.loadFromFile("png/ground2.png");
    groundTexture3.loadFromFile("png/ground3.png");
	lifeTexture.loadFromFile("png/life.png");
    hpTexture.loadFromFile("png/hp.png");
    spikesLTexture.loadFromFile("png/spikesL.png");
    spikesPTexture.loadFromFile("png/spikesP.png");
    backgroundTexture1.loadFromFile("png/background1.png");
    backgroundTexture2.loadFromFile("png/background2.png");
    backgroundTexture3.loadFromFile("png/background3.png");
    backgroundTexture4.loadFromFile("png/background4.png");
    loadCoins();
	loadPortal();
}

//Wyswietlenie mapy
void Map::drawMap(Char &objectChar, float deltaTime){
    
    for(int y = 0; y < 30; y++){
        for(int x = 0; x < 40; x++){
            if(map[x][y] == 'X'){
					Sprite ground;
					ground.setTexture(groundTexture1, 0);
					ground.setPosition(x*20, y*20);
					window.draw(ground);
            }
			if(map[x][y] == 'G'){   
					Sprite ground2;	
					ground2.setTexture(groundTexture2, 0);
					ground2.setPosition(x*20, y*20);
					window.draw(ground2);
            }
            if(map[x][y] == 'Z'){   
					Sprite ground3;
					ground3.setTexture(groundTexture3, 0);
					ground3.setPosition(x*20, y*20);
					window.draw(ground3);
            }
			if(map[x][y] == 'L'){               
					Sprite life;
					life.setTexture(lifeTexture, 0);
					life.setPosition(x*20, y*20);
					window.draw(life);
            }
            if(map[x][y] == 'T'){               
				drawPortal(x, y, deltaTime);
            }
            if(map[x][y] == 'S'){               
					Sprite spikesL;
					spikesL.setTexture(spikesLTexture, 0);
					spikesL.setPosition(x*20, y*20);
					window.draw(spikesL);
            }
            if(map[x][y] == 'D'){             
		            Sprite spikesP;
		            spikesP.setTexture(spikesPTexture, 0);
		            spikesP.setPosition(x*20, y*20);
		            window.draw(spikesP);
            }
            if(map[x][y] == '$'){               
					drawCoin(x, y, deltaTime);
            }
        }
    }
}

//Wyswietlanie licznika czasu
void Map::drawTimer(float gameTime, Font objectFont){
    stringstream stringS (stringstream::in | stringstream::out);
    int tempTime = gameTime * 10;
    gameTime = tempTime * 0.1;
    stringS << gameTime;
    string Timer = "Czas gry: " + stringS.str();
    if(tempTime%10==0) Timer += ".0";
    Text timerText;
    timerText.setFont(objectFont);
    timerText.setCharacterSize(20);
    timerText.setString(Timer);
    timerText.setColor(Color::White);
    timerText.move(350, 50);
    window.draw(timerText);
}

//Wyswietlenie tla
void Map::drawBackground(int &levelActive){
    Sprite background;
    switch(levelActive){
        case 1:
	        background.setTexture(backgroundTexture1, 0);
        break;
        case 2:
            background.setTexture(backgroundTexture2, 0);
        break;
        case 3:
            background.setTexture(backgroundTexture3, 0);
        break;
        case 4:
            background.setTexture(backgroundTexture4, 0);
        break;
    }
	window.draw(background);
}

//Wyswietlenie ilosci punktow
void Map::drawPoints(Char &objectChar, Font objectFont){
    stringstream stringS (stringstream::in | stringstream::out);
    stringS << objectChar.points;
    string Timer = "Points: " + stringS.str();
    Text pointsText;
    pointsText.setFont(objectFont);
    pointsText.setCharacterSize(20);
    pointsText.setString(Timer);
    pointsText.setColor(Color::White);
    pointsText.move(50, 50);
    window.draw(pointsText);
}

//Wyswietlenie ilosci �y�
void Map::drawLife(Char &objectChar){
    Sprite hp;
    hp.setTexture(hpTexture, 0);
    for(int i = 0; i < objectChar.life; i++){
        hp.setPosition(650 + 40*i, 50);
        window.draw(hp);
    }
}

//Wyswietlenie klatki
void Map::drawScene(Char &objectChar, int &levelActive, float gameTime, float deltaTime, double playerX, double playerY, Font ArialFont){
    drawBackground(levelActive);
    drawMap(objectChar, deltaTime);
    drawTimer(gameTime, ArialFont);
    drawLife(objectChar);
    drawPoints(objectChar, ArialFont);
}

//Wczytanie poziomu do tablicy z pliku
void Map::loadLevel(int &levelActive){
    FILE *levelFile;
    errno_t error = 0;

    switch(levelActive){
        case 1:
        error = fopen_s(&levelFile, "1.level", "rb");
        break;
        case 2:
        error = fopen_s(&levelFile, "2.level", "rb");
        break;
        case 3:
        error = fopen_s(&levelFile, "3.level", "rb");
        break;
        case 4:
        error = fopen_s(&levelFile, "4.level", "rb");
        break;
    }

    if(!error){
        for(int y = 0; y < 30; y++){
	        for(int x = 0; x < 42; x++){
		        char letter = getc(levelFile);
		        if(letter != '\n' && letter != '\r'){
                    map[x][y] = letter;
		        }
	        }
        }
    }
}

//Tekstury coinsow
void Map::loadCoins(){
	Texture coinTexture;
	coinTexture.loadFromFile("png/coin/1.png");
    coinsLoad.push_back(coinTexture);
    coinTexture.loadFromFile("png/coin/2.png");
    coinsLoad.push_back(coinTexture);
    coinTexture.loadFromFile("png/coin/3.png");
    coinsLoad.push_back(coinTexture);
    coinTexture.loadFromFile("png/coin/4.png");
    coinsLoad.push_back(coinTexture);
    coinTexture.loadFromFile("png/coin/5.png");
    coinsLoad.push_back(coinTexture);
    coinTexture.loadFromFile("png/coin/6.png");
	coinsLoad.push_back(coinTexture);
}

void Map::loadPortal(){
	Texture portalTexture;
	portalTexture.loadFromFile("png/portal/1.png");
    portalLoad.push_back(portalTexture);
    portalTexture.loadFromFile("png/portal/2.png");
    portalLoad.push_back(portalTexture);
    portalTexture.loadFromFile("png/portal/3.png");
    portalLoad.push_back(portalTexture);
    portalTexture.loadFromFile("png/portal/4.png");
    portalLoad.push_back(portalTexture);
    portalTexture.loadFromFile("png/portal/5.png");
    portalLoad.push_back(portalTexture);
    portalTexture.loadFromFile("png/portal/6.png");
	portalLoad.push_back(portalTexture);
}


//Dla wyswietlania animacji
void Map::nextFrame(int &frame, int amount){
    if(frame < amount - 1){
        frame++;
    }
    else{
        frame = 0;
    }
}

//Wyswietlenie coinsow
void Map::drawCoin(int x, int y, float deltaTime){
    Sprite coin;
    timeCoinFrame += deltaTime;
    if(timeCoinFrame > 0.5){
        timeCoinFrame = 0;
        nextFrame(coinFrame, 6);
    }
    coin.setTexture(coinsLoad[coinFrame]);
    coin.setPosition(x*20, y*20);
    window.draw(coin);
}
//Wyswietlanie portalu
void Map::drawPortal(int x, int y, float deltaTime){
    Sprite portal;
    timePortalFrame += deltaTime;
    if(timePortalFrame > 0.1){
        timePortalFrame = 0;
        nextFrame(portalFrame, 6);
    }
    portal.setTexture(portalLoad[portalFrame]);
    portal.setPosition(x*20, y*20);
    window.draw(portal);
}